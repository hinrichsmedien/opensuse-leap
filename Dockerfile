FROM harbor.hnrx.de/base/opensuse/leap:15.2
LABEL maintainer="Matthias Hinrichs <matthias.hinrichs@me.com>"
LABEL name="SUSE Manager baseimage build profile"
LABEL version="1.0.0"

RUN zypper -n install salt-cloud python3-pip

CMD /bin/bash